This addon adds some Lattice-based utilities to Blender.

# Tweak Lattice
Tweak Lattice lets you create a lattice setup at the 3D cursor to make deformation adjustments to the selected objects.

# Camera Lattice
Camera Lattice lets you create a lattice in a camera's view frame and deform a character (or any collection) with the lattice.

Note: If you want to delete a lattice, make sure to do it through the addon's UI, which is in the sidebar. This will make sure to delete all modifiers, drivers and animation datablocks that were created along with the lattice.